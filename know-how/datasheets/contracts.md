# Contracts



| Name | Network | Address                                      | ABI                                                                                                                        | Notes                                                                            |
| ---- | ------- | -------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| VTHO | main    | `0x0000000000000000000000000000456E65726779` | [https://github.com/vechain/b32/blob/master/ABIs/energy.json](https://github.com/vechain/b32/blob/master/ABIs/energy.json) | [https://github.com/zzGHzz/thor-builtin](https://github.com/zzGHzz/thor-builtin) |
| VTHO | test    | `0x0000000000000000000000000000456E65726779` | [https://github.com/vechain/b32/blob/master/ABIs/energy.json](https://github.com/vechain/b32/blob/master/ABIs/energy.json) | [https://github.com/zzGHzz/thor-builtin](https://github.com/zzGHzz/thor-builtin) |

### Links <a href="#user-content-links" id="user-content-links"></a>

* [vechain/b32@github](https://github.com/vechain/b32) maintains a list of known and public signatures to be used for everyone.
* [List of known contracts & addresses from seevechain.com](https://github.com/nodatall/seevechain/blob/master/shared/knownAddresses.js)
