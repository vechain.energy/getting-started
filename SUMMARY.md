# Table of contents

* [Getting Started on VeChain](README.md)

## Know How

* [What is VeChain?](know-how/what-is-vechain.md)
* [What is Fee Delegation?](know-how/what-is-fee-delegation.md)
  * [How does Fee Delegation work?](know-how/what-is-fee-delegation/how-does-fee-delegation-work.md)
  * [How to implement Fee Delegation?](know-how/what-is-fee-delegation/how-to-implement-fee-delegation.md)
  * [How to deploy with Fee Delegation?](know-how/what-is-fee-delegation/how-to-deploy-with-fee-delegation.md)
* [Datasheets](know-how/datasheets/README.md)
  * [APIs](know-how/datasheets/apis.md)
  * [Contracts](know-how/datasheets/contracts.md)
  * [DEX](know-how/datasheets/dex.md)
  * [Explorers](know-how/datasheets/explorers.md)
  * [Links](know-how/datasheets/links.md)
  * [Nodes](know-how/datasheets/nodes.md)
  * [SDKs](know-how/datasheets/sdks.md)
  * [Wallets](know-how/datasheets/wallets.md)

## how-to

* [Create a Wallet](how-to/create-a-wallet/README.md)
  * [Sync2](how-to/create-a-wallet/sync2.md)
  * [shell + openssl](how-to/create-a-wallet/shell-+-openssl.md)
  * [Faucet / First Tokens](how-to/create-a-wallet/faucet-first-tokens.md)
* [Connect with Browser](how-to/how-to-connect-to-the-blockchain-in-the-browser.md)
* [Connect with Node.js](how-to/how-to-connect-to-the-blockchain-using-node.js.md)
* [Connect with python](how-to/connect-with-python.md)
* [Setup a project with Hardhat](how-to/setup-a-project-with-hardhat/README.md)
  * [Deploy an OpenZeppelin-Contract](how-to/setup-a-project-with-hardhat/deploy-an-openzeppelin-contract.md)
  * [Upgradable Contracts with OpenZeppelin](how-to/setup-a-project-with-hardhat/upgradable-contracts-with-openzeppelin.md)
* [Transaction Dependency](how-to/transaction-dependency.md)
* [Support Sync-Browser and Sync2(-Lite)](how-to/support-sync-browser-and-sync2-lite.md)
* [Analytics](how-to/analytics.md)
* [Node](how-to/node.md)
  * [Setup Public Node](how-to/node/setup-public-node.md)

## coding

* [Example Projects for study](coding/readme.md)
* [by Language](coding/by-language/README.md)
  * [Javascript](coding/by-language/javascript.md)
  * [Python](coding/by-language/python/README.md)
    * [Read Events on every new Block](coding/by-language/python/read-events-on-every-new-block.md)
    * [Deploy Contract](coding/by-language/python/deploy-contract.md)
    * [get owned token ids for NFT contract](coding/by-language/python/get-owned-token-ids-for-nft-contract.md)
    * [Frequently Passed Problems](coding/by-language/python/frequently-passed-problems.md)
* [Misc. Snippets](coding/misc.-snippets/README.md)
  * [call contract with curl](coding/misc.-snippets/call-contract-with-curl.md)
  * [generate private key from shell](coding/misc.-snippets/generate-private-key-from-shell.md)
* [Helpful Links](coding/helpful-links.md)

## vechain.energy

* [Contract-Verification](vechain.energy/contract-verification.md)
* [Contract-Rate-Limits](vechain.energy/contract-rate-limits.md)
* [Signing API](vechain.energy/signing-api.md)
* [REST API](vechain.energy/rest-api/README.md)
  * [Transaction](vechain.energy/rest-api/transaction.md)
  * [Call (Reading)](https://testnet.vechain.energy/docs/api/call)
  * [Events & Logs](https://testnet.vechain.energy/docs/api/event)
* [Changelog](vechain.energy/changelog.md)
* [Privacy Statement](vechain.energy/privacy-statement.md)
