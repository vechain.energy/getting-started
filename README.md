# Getting Started on VeChain

_This repository provides information and instructions for developers to get started with development in the VeChain-ecosystem._

_Do you want to connect, tap other developers or share your knowledge? Join us on_ [_Discord_](https://discord.gg/dhVCVNbHRT)_._

_This page is currently available at:_ [_https://learn.vechain.energy/_](https://learn.vechain.energy/)__

_You can suggest edits on GitLab at:_ [_https://gitlab.com/vechain.energy/getting-started/-/tree/main_](https://gitlab.com/vechain.energy/getting-started/-/tree/main)__
